//
//  SceneDelegate.h
//  objective
//
//  Created by Hazem on 5/24/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

